fileHandle = open("preproinsulin-seq.txt", "r")
#print(fileHandle.read())
origFileContent = fileHandle.read()

import re
import string

#Cleaning preproinsulin-seq.txt programmatically
#find and replace using regex in python
stripSpaces = re.sub("\s", "", origFileContent)
stripNewlineChar = re.sub("\\n", "", stripSpaces)
stripOrigin = re.sub("ORIGIN", "", stripNewlineChar)
stripDoubleSlash = re.sub("\//", "", stripOrigin)
stripNumbers = re.sub("[0-9]", "", stripDoubleSlash)
print(stripNumbers)

#get the length of the data
number_of_characters = len(stripNumbers)
print('Number of characters in text file :', number_of_characters)

#Write the clean data to a new file 
fileHandle1 = open("preproinsulin-seq-clean.txt", "w")
fileHandle1.write(stripNumbers)
fileHandle1.close()
print ("*****************************************************")

#Exercise 2: Obtaining the protein sequence of human insulin
fileHandle2 = open("preproinsulin-seq-clean.txt", "r")
fetchAmino = fileHandle2.read()
print(fetchAmino)
print(len(fetchAmino))


# Printing Amino Acids 55-89 90-110
print("AMINO ACIDS")
def splitFunc(fetchAmino):
    comb_str1 = ""
    comb_str2 = ""
    comb_str3 = ""
    comb_str4 = ""
    for i in range(0, len(fetchAmino)):
        if (i>=0 and i < 24):
            comb_str1 += fetchAmino[i]
            fileHandle_Is = open("Isinsulin-seq-clean.txt", "w")
            fileHandle_Is.write(comb_str1)
            fileHandle_Is.close()
        elif (i >= 24 and i<54):
            comb_str2 += fetchAmino[i]
            fileHandle_b = open("binsulin-seq-clean.txt", "w")
            fileHandle_b.write(comb_str2)
            fileHandle_b.close()
        elif (i >= 54 and i<89):
            comb_str3 += fetchAmino[i]
            fileHandle_c = open("cinsulin-seq-clean.txt", "w")
            fileHandle_c.write(comb_str3)
            fileHandle_c.close()
        elif (i >=89 and i<=110):
            comb_str4 += fetchAmino[i]
            fileHandle_a = open("ainsulin-seq-clean.txt", "w")
            fileHandle_a.write(comb_str4)
            fileHandle_a.close()

    print("Amino Acids 1-24 : {}".format(comb_str1))
    print("Length of the string is : {}".format(len(comb_str1)))

    print("Amino Acids 25-54 : {}".format(comb_str2))
    print("Length of the string is : {}".format(len(comb_str2)))
        
    print("Amino Acids 55-89 : {}".format(comb_str3))
    print("Length of the String is : {}".format(len(comb_str3)))
    
    print("Amino Acids 90-110 : {}".format(comb_str4))
    print("Length of the string is : {}".format(len(comb_str4)))
splitFunc(fetchAmino)